import { Component } from '@angular/core';

@Component({
  selector: 'app-clases',
  templateUrl: './clases.component.html',
  styleUrls: ['./clases.component.css']
})
export class ClasesComponent {

  isInfo: Boolean = true;
  isLoading: Boolean = false;

  properties: Object = {
    success : true
  };

  constructor() { }

  makeItDanger () {
    return this.isInfo = !this.isInfo;
  }

  doTextChange () {
    return this.properties['success'] = !this.properties['success'];
  }

  saveSomething () {
    this.isLoading = true;
    setTimeout( () => {
      this.isLoading = !this.isLoading;
    }, 3000);
  }

}
