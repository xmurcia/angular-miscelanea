import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-style',
  template: `
    <p [style.fontSize.px]="tamany" [ngStyle]="decorator">
      Hola Mundo ...esta es una etiqueta
    </p>

    <button class="btn btn-primary" (click)="tamany = tamany+5; changeColor()">
      <i class="fa fa-plus"></i>
    </button>

    <button class="btn btn-primary" (click)="tamany = tamany-5; changeColor()">
      <i class="fa fa-minus"></i>
    </button>

  `,
  styles: []
})
export class NgStyleComponent implements OnInit {

  tamany: Number = 20;
  decorator: Object = {
    color :  'blue',
  };

  changeColor () {
    const colors: Array<string> = ['red', 'gray', 'purple', 'pink'];
    const chosen: any = Math.round( Math.random() * 3 );
    if ( this.decorator['color'] === colors[chosen]) {
      console.log('decorator ', this.decorator['color'] + ' color', colors[chosen]);
      this.changeColor();
    } else {
      this.decorator['color'] = colors[chosen];
    }
  }

  constructor() { }

  ngOnInit() {
  }

}
