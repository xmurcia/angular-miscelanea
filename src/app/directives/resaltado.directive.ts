import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appResaltado]'
})
export class ResaltadoDirective {

  @Input() appResaltado;

  constructor(private el: ElementRef) { }

  @HostListener('mouseenter') mouseIn () {
    this.el.nativeElement.style.backgroundColor = this.appResaltado || 'pink';
  }

  @HostListener('mouseleave') mouseOut () {
    this.el.nativeElement.style.backgroundColor = 'transparent';
  }

}
